# metalgen: Generating metal band names in Tensorflow

`metalgen` is a Tensorflow model designed to generate
band names for metal bands based on artist names
and genres from the [Metal Archives](https://www.metal-archives.com/).

The code is modified from minimaxir's [textgenrnn](https://github.com/minimaxir/textgenrnn)
library, and is released under the same MIT license.
