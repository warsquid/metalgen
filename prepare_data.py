from pathlib import Path
from typing import List
import pandas as pd


def read_genres(filename):
    genres = []
    with open(filename) as file_in:
        for line in file_in:
            if stripped_line := line.strip():
                genres.append(stripped_line)
    return genres


def read_tags(filename: str) -> pd.DataFrame:
    tags = pd.read_csv(
        filename,
        sep='\t',
        names=['tag_id', 'genre', 'tag_count']
    )
    # Drop missing tags
    tags = tags.loc[~ tags['genre'].isna(), :]
    return tags


def read_artists(filename: str) -> pd.DataFrame:
    # TODO: May want to read other columns from this file
    artists = pd.read_csv(filename,
                          sep='\t',
                          header=None,
                          index_col=False,
                          usecols=[0, 1, 2])
    artists.columns = ['artist_id', 'artist_hash', 'artist_name']
    return artists


def read_tagged_artists(filename: str) -> pd.DataFrame:
    tagged = pd.read_csv(filename, sep='\t', header=None)
    tagged.columns = ['artist_id', 'tag_id', 'status', 'date']
    return tagged


def get_artist_tag_info(main_dir: str,
                        deriv_dir: str,
                        drop_various: bool = True) -> pd.DataFrame:
    tag_filename = Path(deriv_dir, 'tag')
    tags = read_tags(tag_filename)

    artists_filename = Path(main_dir, 'artist')
    artists = read_artists(artists_filename)

    tagged_artists_filename = Path(deriv_dir, 'artist_tag')
    tagged_artists = read_tagged_artists(tagged_artists_filename)

    # Merge artists and tag names
    tagged_artists = (
        tagged_artists
            .merge(artists[['artist_id', 'artist_name']], on='artist_id')
            .merge(tags, on='tag_id')
    )

    if drop_various:
        tagged_artists = tagged_artists.query('~ (artist_name == "Various Artists")')

    tagged_artists = (
        tagged_artists.sort_values('artist_id')
            .set_index('artist_id')
    )
    return tagged_artists


def get_metal_artists(tagged_artists: pd.DataFrame,
                      core_genres: List[str],
                      sub_genres: List[str]) -> pd.DataFrame:
    def _get_genres(df, genre_list):
        found_genres = [genre for genre in genre_list
                        if df['genre'].str.contains(genre).any()]
        return found_genres

    core_patterns = '|'.join(core_genres)
    has_core = (
        tagged_artists.groupby('artist_id')
        ['genre'].apply(
            lambda x: x.str.contains(core_patterns).any()
        )
    )

    tagged_metal = tagged_artists.loc[has_core, :]

    all_genres = core_genres + sub_genres
    metal_artists = (
        tagged_metal.groupby('artist_name')
            .apply(_get_genres, all_genres)
            .rename('genre_list')
            .reset_index()
    )
    return metal_artists
